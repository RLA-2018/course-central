# Refugee Learning Accelerator

Here you will find course materials for the Refugee Learning Accelerator, a project of the MIT Media Lab. 

We ran this course during the Fall of 2017 for several dozen computer scientists, all based in the Middle East. The course was structured around three design challenges, each of which lasted two weeks. In addition to the materials you find here, teams participated in a series of video calls--hosted on [Unhangout](https://unhangout.media.mit.edu/)--with various experts, both technical and humanitarian. At the end of each two week sprint, teams submitted a very preliminary prototype. 

The 14 most promising teams were invited to a in-person workshop in Amman, Jordan in January 2018. You can learn more about the Refugee Learning Acceleratore [here](https://refugeelearning.media.mit.edu/).

Questions? refugeelearning@media.mit.edu